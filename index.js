const axios = require("axios");
const express = require("express");

const serverPort = 80;

const getData = (url) => {
	return new Promise((resolve, reject) => {
		axios.get(url)
			.then((res) => {
				if (res.status == 200) {
					resolve(res.data);
				}
				else {
					reject(res.statusText);
				}
			})
			.catch((err) => {
				console.log(err);
				reject(err);
			});
	});
};

// Credit: https://stackoverflow.com/questions/979256/sorting-an-array-of-objects-by-property-values
const sort_by = (field, reverse, primer) => {
	const key = primer ?
		function(x) {
			return primer(x[field]);
		} :
		function(x) {
			return x[field];
		};
	reverse = !reverse ? 1 : -1;
	return function(a, b) {
		return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	};
};

const getApi = (sort, endpoint) => {
	return new Promise((resolve, reject) => {
		let res = [];
		let data = [];

		getData(endpoint)
			.then((first) => {
				let pageCount = Math.ceil(first["count"] / 10);
				for (let i = 0; i < pageCount; i++) {
					if (i == 0) continue;
					let x = i + 1;
					data.push(getData(endpoint + "?page=" + x));
				}

				Promise.allSettled(data).then((values) => {
					for (let i in first["results"]) {
						res.push(first["results"][i]);
					}

					values.forEach((item) => {
						for (let i in item["value"]["results"]) {
							res.push(item["value"]["results"][i]);
						}
					});

					if (sort) {
						switch (sort) {
						case "height":
							resolve(res.sort(sort_by(sort, true, parseInt)));
							break;
						case "mass":
							resolve(res.sort(sort_by(sort, true, parseInt)));
							break;
						case "name":
							resolve(res.sort(sort_by(sort, false, null)));
							break;
						}
						resolve();
					} else {
						resolve(res);
					}
						
					resolve(res);
				});
			})
			.catch((err) => {
				console.error(err);
				reject(err);
			});

	});
};

const getResidents = (residentsObj) => {
	return new Promise((resolve) => {
		let temp = [];
		let res = [];
		residentsObj.forEach((item) => {
			temp.push(getData(item));
		});
		resolve(Promise.allSettled(temp).then((values) => {
			values.forEach((item) => {
				res.push(item["value"]["name"]);
			});
			return res;
		}));
	});
};

const main = () => {
	let app = express();

	app.get("/people", (req, res) => {
		getApi(req.query["sortBy"], "https://swapi.dev/api/people")
			.then((data) => {
				let ret = JSON.stringify(data, null, 4);
				res.status(200);
				res.type("json");
				res.send(ret);
			})
			.catch((e) => {
				console.error(e);
				res.sendStatus(500);
			});
	});

	app.get("/planets", (req, res) => {
		getApi(null, "https://swapi.dev/api/planets") // Sorting was stated as not-required so only the commonly shared key "name" will be sortable.
			.then((data) => {
				let temp = [];
				for (let i in data) {
					temp.push(getResidents(data[i]["residents"]));
				}
				Promise.allSettled(temp).then((values) => {
					for (let i in values) {
						data[i]["residents"] = values[i]["value"];
					}
					res.status(200);
					res.type("json");
					res.send(data);
				});
			})
			.catch((e) => {
				console.error(e);
				res.sendStatus(500);
			});
	});
	
	app.get("/*", (req, res) => {
		res.sendStatus(404);
	});

	const server = app.listen(serverPort, () => {
		console.log("Listening on port " + server.address().port);
	});
};

if (require.main === module) {
	main();
}